//soal 1
console.log('Luas Lingkaran')
const pi = 3.14
let  luasLingkaran = (jarijari) => { return pi*jarijari*jarijari;};
console.log(luasLingkaran(10));

console.log('Keliling Lingkaran')
const pi = 3.14
let  kelilingLingkaran = (jarijari) => { return 2*pi*jarijari;};
console.log(kelilingLingkaran(10));


//soal 2
function tambahKata()
{
    const kata1 = 'saya'
    const kata2 = 'adalah'
    const kata3 = 'seorang'
    const kata4 = 'fronted'
    const kata5 = 'developer'

    const theString = `${kata1} ${kata2} ${kata3} ${kata4} ${kata5}`

    return theString;
}
let kalimat = tambahKata()

console.log(kalimat)


//soal 3
literal = (firstName, lastName) =>{

    const theString = `${firstName} ${lastName}`

    return theString;
    }
    console.log(literal('Wiliam','Imoh'))


//soal 4
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

  const {firstName, lastName, destination, occupation, spell} = newObject
  console.log(firstName, lastName, destination, occupation, spell)



//soal 5
const west = ['Will', 'Chris', 'Sam', 'Holly']
const east = ['Gill', 'Brian', 'Noel', 'Maggie']

let combinedArray = [...west, ...east] 
console.log(combinedArray)

    